(function() {
  'use strict';

  /**
   * @ngdoc overview
   * @name fondoinvers.dataSearch
   * @requires ui.router
   *
   * @description
   * Module for the dataSearch section.
   */
   angular.module('fondoinvers.dataSearch', [
     /*Dependecia entre Módulos*/

     'fondoinvers.core',
     'fondoinvers.searchResults',
     'fondoinvers.shared',

     /*Fin Dependecia entre Módulos*/

     'bankia.ui.button',
     'bankia.ui.checkbox',
     'bankia.ui.combo',
     'bankia.ui.date',
     'bankia.ui.device',
     'bankia.ui.onresize',
     'bankia.ui.pagination',
     'bankia.ui.radio',
     'bankia.ui.statebutton',
     'bankia.cl.ui.document',
     'bankia.cl.ui.flows',
     'bankia.ui.select',
     'bankia.cl.ui.iban',
     'bankia.cl.ui.ccc',
     'bankia.cl.ui.multiproduct',
     'bankia.cl.ui.product',
     'bankia.ui.tasklist',
     'bankia.ui.loading',
     /*'bankia.ui.snackbar',*/
     'bankia.core.scenario',
     'bankia.core.sap',
     'bankia.ui.message',
     'atmira.ui.select', //Componente atmira
     /*'ngResource',*/
     'ui.router',
     /*'ngAnimate',*/
     'ui.bootstrap'
   ])

   .config(['clSearchCustomerConfProvider', function(clSearchCustomerConfProvider) {
        //Si no estoy en NEO, emulo una respuesta
        if(!angular.isDefined(window.getIDProceso)) {
            clSearchCustomerConfProvider.config({
                respuestaBeanTipoDocumento: {
                    numDoc:'6Y',
                    codTipoDoc:'1',
                    titular:'### ALBERTO PEREZ SEGUNDO',
                    nombreApellidos:'### ALBERTOÛPEREZÚSEGUNDO',
                    titularRiesgos:'### ALBERTO*PEREZÚSEGUNDO',
                    tipoDoc:'DNI',
                    numCli:'000696609',
                    tipoPersona:'1',
                    fechaNac: new Date('1956-04-23'),
                    entidadRepresentada:'00000',
                    domicilio:'C. ALCALA, 100',
                    poblacion:'MADRID',
                    codPostal:'28010'
                }
            });
        }
    }])

   .config(['clSearchCustomerConfProvider', function(clSearchCustomerConfProvider) {
        //Si no estoy en NEO, emulo una respuesta
        if(!angular.isDefined(window.getIDProceso)) {
            clSearchCustomerConfProvider.config({
                respuestaBeanCodigoCuentaCliente: {
                    numDoc:'6Y',
                    codTipoDoc:'1',
                    titular:'SARAIX LOPEZ LOPEZ',
                    nombreApellidos:'SARAIXÛLOPEZÚLOPEZ',
                    titularRiesgos:'SARAIX*LOPEZÚLOPEZ',
                    tipoDoc:'DNI',
                    numCli:'570606',
                    tipoPersona:'1',
                    telefono:'918878788',
                    fechaNac: new Date('1980-12-10'),
                    entidadRepresentada:'00000',
                    domicilio:'PZA. ORFILA, 4 ESC. 1 3 B NINGUNO',
                    poblacion:'BARCELONA',
                    codPostal:'08030',
                    codEntidad:'00000',
                    descEntidad:'BANKIA',
                    codTipoProducto:'10020',
                    descTipoProducto:'AH.PLAZO 6 MESES','situacion':'ACTIVA',
                    numContrato:'6920200000567',
                    ccc: {
                        bankCode:'2038',
                        branchCode:'0692',
                        checkDigits:'30',
                        accountNumber:'0200000567'
                    }
                }
            });
        }
    }])

    .config(['clSearchCustomerConfProvider', function(clSearchCustomerConfProvider) {
        //Si no estoy en NEO, emulo las respuestas
        if(!angular.isDefined(window.getIDProceso)) {
            clSearchCustomerConfProvider.config({
                respuestaBeanCodigoCuentaCliente: {
                    numDoc:'6Y',
                    codTipoDoc:'1',
                    titular:'SARAIX LOPEZ LOPEZ',
                    nombreApellidos:'SARAIXÛLOPEZÚLOPEZ',
                    titularRiesgos:'SARAIX*LOPEZÚLOPEZ',
                    tipoDoc:'DNI',
                    numCli:'570606',
                    tipoPersona:'1',
                    telefono:'918878788',
                    fechaNac: new Date('1980-12-10'),
                    entidadRepresentada:'00000',
                    domicilio:'PZA. ORFILA, 4 ESC. 1 3 B NINGUNO',
                    poblacion:'BARCELONA',
                    codPostal:'08030',
                    codEntidad:'00000',
                    descEntidad:'BANKIA',
                    codTipoProducto:'10020',
                    descTipoProducto:'AH.PLAZO 6 MESES','situacion':'ACTIVA',
                    numContrato:'6920200000567',
                    ccc: {
                        bankCode:'2038',
                        branchCode:'0692',
                        checkDigits:'30',
                        accountNumber:'0200000567'
                    }
                }
            });
            clSearchCustomerConfProvider.config({
                respuestaBeanNumeroContrato: {
                    'numDoc':'6Y',
                    'codTipoDoc':'1',
                    'titular':'ALEJANDRO MARTIN GIL',
                    'nombreApellidos':'ALEJANDROÛMARTINÚGIL',
                    'titularRiesgos':'ALEJANDRO*MARTINÚGIL',
                    'tipoDoc':'DNI',
                    'numCli':'673780',
                    'tipoPersona':'1',
                    'telefono':'915685689',
                    'fechaNac':new Date('1973-04-23'),
                    'entidadRepresentada':'00000',
                    'domicilio':'C. RIAZOR, 12 1 C',
                    'poblacion':'CORUÑA LA',
                    'codPostal':'15001',
                    'codEntidad':'00000',
                    'descEntidad':'BANKIA',
                    'codClaseProducto':{
                        'DELIM':' ',
                        'fieldCodigo':'20',
                        'fieldCorto':'FINANCIACION',
                        'fieldLargo':'NO'
                    },
                    'descClaseProducto':'FINANCIACION',
                    'codSubclaseProducto':{
                        'DELIM':' ',
                        'fieldCodigo':'2010',
                        'fieldCorto':'PRESTAMOS',
                        'fieldLargo':'NO'
                    },
                    'descSubclaseProducto':'PRESTAMOS',
                    'codTipoProducto':'21000',
                    'descTipoProducto':'PRESTAMO',
                    'saldoDisponible':{
                        'val':'47197.48',
                        'currency':{
                            'cod':'2811',
                            'ad':'EUR',
                            'sd':'EUR',
                            'ld':'EURO'
                        }
                    },
                    'situacion':'ACTIVA',
                    'numContrato':'6605170',
                    'ccc':null}
            });
        }
    }]);


 })();

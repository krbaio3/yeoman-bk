var probandoComponent = {
      bindings: {
        tipoEntidadArray : '<',
        tipoEntidad : '&',
        document : '<',
        razonSocial : '&'
      },
      controllerAs: 'dataSearchCtrl',
      transclude: true,

      controller: function ($window) {
        var vm = this;

        vm.alert = function() {
            $window.alert('Esto es un alert2');
            console.log('Ha entrado2');
        };
      },
      templateUrl:'src/app/components/dataSearch.module/nifSearchView.html'
};


  angular
      .module('fondoinvers.dataSearch.ejemplo', [])
        .component('probandocomponent', probandoComponent);

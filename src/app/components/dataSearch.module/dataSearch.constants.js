(function() {
  'use strict';

  angular
    .module('fondoinvers.dataSearch')
    .constant('dataSearchConstans', {
        documento: 'Documento: ',
        nombre: 'Nombre y apellidos / Razón social: '
    });
})();

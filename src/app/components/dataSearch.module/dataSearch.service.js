(function() {
    'use strict';

    angular
        .module('fondoinvers.dataSearch')
        .service('dataSearchSrv', dataSearchSrv);

    function dataSearchSrv($http, $q) {

        function getConsultarRepartosFondosInversionCliente(entidad, tipoDocumento, numeroDocumento) {

            var deffered = $q.defer();

            var data = {
                entidad: entidad,
                tipoDocumento: tipoDocumento,
                numeroDocumento: numeroDocumento
            };

            function consultaSuccess(data) {
                deffered.resolve(data);
            }

            function consultaError(error) {
                deffered.reject(error);
            }

            //$http.post('http://10.11.1.124:8088/api/1.0/fondos/repartos/ConsultarRepartosFondosInversionClienteSBP/1.0?TGT=&x-j_gid_cod_app=O2', data).success(consultaSuccess).error(consultaError);
            consultaSuccess(data);

            return deffered.promise;

        }

        return {
            getConsultarRepartosFondosInversionCliente: getConsultarRepartosFondosInversionCliente
        }

    }

})();

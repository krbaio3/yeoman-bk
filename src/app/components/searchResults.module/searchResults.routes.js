(function() {
  'use strict';

  angular
    .module('fondoinvers.searchResults')
    .config(configuration);

    /* @ngInject */
    function configuration($stateProvider) {
      $stateProvider
        .state('searchResults', {
          url: '/searchResults',
          templateUrl: 'app/components/searchResults.module/searchResults.component.html',
          controller: 'searchResultsController',
          controllerAs: 'searchResultsCtrl'
        });
    }
  })();

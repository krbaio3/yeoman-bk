(function() {
'use strict';

angular.module('fondoinvers.searchResults', [
  /*Dependecia entre Módulos*/

  'fondoinvers.core',
  'fondoinvers.dataSearch',
  'fondoinvers.historyDeals',

  /*Fin Dependecia entre Módulos*/

  'bankia.ui.button',
  'bankia.ui.checkbox',
  'bankia.ui.combo',
  'bankia.ui.date',
  'bankia.ui.device',
  'bankia.ui.onresize',
  'bankia.ui.pagination',
  'bankia.ui.radio',
  'bankia.ui.statebutton',
  'bankia.cl.ui.document',
  'bankia.cl.ui.flows',
  'bankia.ui.select',
  'bankia.cl.ui.iban',
  'bankia.cl.ui.ccc',
  /*'bankia.ui.tabletree',*/
  'bankia.ui.tasklist',
  'bankia.ui.loading',
  /*'bankia.ui.snackbar',*/
  'bankia.core.scenario',
  'bankia.core.sap',
  'bankia.ui.message',
  'atmira.ui.select', //Componente atmira
  /*'ngResource',*/
  'ui.router',
  /*'ngAnimate',*/
  'ui.bootstrap']);
})();

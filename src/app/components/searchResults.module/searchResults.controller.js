(function () {
  'use strict';

  angular
  .module('fondoinvers.searchResults')
  .controller('searchResultsController', ['$scope', '$q', '$timeout', '$http', '$state', 'serviciosEjemplo',
    function ($scope, $q, $timeout, $http, $state, serviciosEjemplo) {

    var vm = this;
    vm.active;
    vm.verDetalle;
    vm.habilitarBotonDetalle;
    vm.historicoRepartosArrayv = serviciosEjemplo.getHistoricoRepartos();
    //vm.showDiv = false;
    /* Datos de ejemplo de los campos de la página de resultados de busqueda */
    vm.clientesArray = serviciosEjemplo.getClientes();

    /* Datos de ejemplo para la tabla de reparto de fondos de destino */
    vm.repartoFondosDestinoArray = serviciosEjemplo.getRepartoFondosDest();
    /* Datos de ejemplo para el contenedor dinamico */
    vm.cuadroDinamicoArray = serviciosEjemplo.getCuadroDinamicoArray();

    $scope.serviceDatosCliente = serviciosEjemplo.getDatosCliente();

    vm.btnactive = true;




    vm.changeClass = function (indexRow) {


      vm.verDetalle = false;
      $scope.indiceDin = indexRow;
      vm.active = indexRow;
      vm.finalizarResultados = true;
      vm.habilitarBotonDetalle = true;
    }

    vm.changeTemplate = function (template) {

      if (template=='porcentaje') {
        vm.porcentaje = true;
        vm.participaciones = false;
      } else {
        vm.porcentaje = false;
        vm.participaciones = true;
      }

    }

    vm.eventoBotonDetalle = function () {

      if (vm.habilitarBotonDetalle==true) {
        vm.habilitarBotonDetalle = false;
        vm.verDetalle = true;
      } else {
        vm.habilitarBotonDetalle = true;
        vm.verDetalle = false;
      }
    }

    $http.get('app/assets/data/document-injection.json').then(onLoadSuccessful, onLoadSuccessError);

    function onLoadSuccessful (response) {
      $scope.documentInjection = response.data;
      //console.log('Document component data load successful');
      //console.log($scope.documentInjection);
    }

    function onLoadSuccessError () {
      $scope.documentInjection = [];
      throw 'Error on loading Document Injection Data';
    }

    $scope.documentInfo = [
    {
     "name" : "Documento 1",
     "type" : "Importante",
     "selectedAction" : "Visualizar"
   },
   {
     "name" : "Documento 2",
     "type" : "Muy importante",
     "selectedAction" : "Visualizar"
   },{
     "name" : "Documento 3",
     "type" : "Mas importante",
     "selectedAction" : "Visualizar"
   },{
     "name" : "Documento 4",
     "type" : "Mucho más importante",
     "selectedAction" : "Visualizar"
   }
   ];

   $scope.documents =
   [
    {
        "index" : 0,
        "id": "Documentación testamentaria",
        "expand": false,
        "state": "En expediente",
        "selectedAction" : "Visualizar"
    },
    {
        "index" : 1,
        "id": "Justificante de reparto",
        "expand": false,
        "state": "Pendiente",
        "selectedAction" : "Digitalizar"
    },

  ];


   $scope.availableTasks = [
   {"taskGroup" : [
   {"name" : "Visualizar"},
   {"name" : "Digitalizar"}
   ]},
   {"taskGroup" : [
   {"name" : "Eliminar"},
   {"name" : "Confirmar validez"},
   {"name" : "Exceptuar"}
   ]},
   {"taskGroup" : [
   {"name" : "Imprimir"},
   {"name" : "Descargar"},
   {"name" : "Adjuntar"}
   ]}
   ];

   $scope.currentNode = null;

 }]);

})();

(function() {
	'use strict';
/**
	* @ngdoc overview
	* @name fondoinvers.historyDeals
	* @requires fondoinvers.core
	* @requires fondoinvers.searchResults
	* @requires ui.router
	*
	* @description
	* Module for the historyDeals section.
*/
   angular.module('fondoinvers.historyDeals', ['ui.router',
   	//'atmira.ui.select',
		'fondoinvers.core',
		'fondoinvers.searchResults'
]);

 })();

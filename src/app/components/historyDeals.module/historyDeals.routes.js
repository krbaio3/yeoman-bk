(function() {
  'use strict';

  angular
    .module('fondoinvers.historyDeals')
    .config(configuration);

    /* @ngInject */
    function configuration($stateProvider) {
      $stateProvider
        .state('historyDeals', {
          url: '/historyDeals',
          templateUrl: 'app/components/historyDeals.module/historyDeals.component.html',
          controller: 'historyDealsController',
          controllerAs: 'historyDealsCtrl'
        });
    }
  })();

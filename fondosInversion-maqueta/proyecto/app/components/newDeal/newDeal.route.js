(function() {
  'use strict';
  
  angular
  .module('fondoinvers.newDeal')
  .config(configuration);
  
  /* @ngInject */
  function configuration($stateProvider) {
    $stateProvider
    .state('main.newDeal', {
      url: '/newDeal',
      templateUrl: 'components/newDeal/newDeal.component.html',
      controller: 'newDealController',
      controllerAs: 'newDealCtrl'
    })
  }
})();
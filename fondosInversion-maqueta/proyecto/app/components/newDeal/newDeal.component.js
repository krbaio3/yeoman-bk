(function () {
  'use strict';

  angular
  .module('fondoinvers.newDeal')
  .controller('newDealController', newDealController);


  function newDealController($scope, $state, serviciosEjemplo){

    /*var vm = this;
    vm.active;

    vm.datosCliente = serviciosEjemplo.getDatosCliente();
    

    vm.historyDealsArray = serviciosEjemplo.getHistoricoRepartos();

    vm.changeClass = function (indexRow) {

        vm.active = indexRow;
        vm.habilitarBotonDetalle = true;
    }

    vm.comprobarhistoryDeals = function () {
        vm.historyDealsArrayFin = new Array();

        for (var i = 0; i < vm.historyDealsArray.length; i++) {

          if (vm.datosCliente.nif == vm.historyDealsArray[i].nif) {
              vm.historyDealsArrayFin.push(vm.historyDealsArray[i]);
          }

        };


    }*/
    $scope.triggerUpdateEvent = function () {
        $scope.$broadcast('updateTaskListRequested', $scope.tasksCollection);
    };

    $scope.triggerSelectTaskEvent = function (task) {
        $scope.$broadcast('selectTaskRequested', task);
    };

        // Sample selected task from example 5 (with bkUseEvents='true' for SIP)
        $scope.$on('selectedTaskFromTaskList', function (event, args) {
            console.log('Event selectedTaskFromTaskList captured with args: ', args);
            $timeout(function () {
                $scope.task5 = args;
                $scope.task6 = args;
                $scope.triggerSelectTaskEvent(args);
            }, 250);
        });
        
        $scope.tasksCollection = [
        {
            description: 'Datos básicos',
                index: 0, // orden
                isEnabled: true,
                isRequired: true,
                isShared: true,
                needsCheckout: false,
                level: -1, // -1, 0, 1
                status: 'done',
                tasks: [{
                    description: 'Productos',
                    index: 0,
                    ownerCode: '1001',
                    ownerDescription: ' - Gran Vía',
                    isEnabled: false,
                    isRequired: true,
                    isShared: true,
                    isInitial: true,
                    level: -1,
                    status: 'done',
                    idSubproceso: '993009f14937538f70feb5c947671bec',
                    codigoSubproceso: 'TareaProductos'
                }, {
                    description: 'Intervinientes',
                    index: 0,
                    ownerCode: '1001',
                    ownerDescription: ' - Gran Vía',
                    isEnabled: false,
                    isRequired: true,
                    isShared: true,
                    level: -1,
                    status: 'undone',
                    idSubproceso: '71b366c98e6129f58dab6a955b937769',
                    codigoSubproceso: 'TareaIntervinientes'
                }, {
                    description: 'Documentación y firma de varios intervinientes',
                    index: 0,
                    // ownerCode: '1001',
                    // ownerDescription: ' - Gran Vía',
                    isEnabled: false,
                    isRequired: true,
                    isShared: true,
                    level: -1,
                    status: 'done',
                    idSubproceso: 'c048ba67ef260c7790d5341e079b151a',
                    codigoSubproceso: 'TareaDocumentacionFirma'
                }]
            }, {
                description: 'Reparto',
                index: 0, // orden
                isEnabled: true,
                isRequired: true,
                isShared: true,
                level: -1, // -1, 0, 1
                status: 'undone',
                tasks: [{
                    description: 'Resolución',
                    index: 0, // orden
                    ownerCode: '1001',
                    ownerDescription: ' - Gran Vía',
                    isEnabled: true,
                    isRequired: true,
                    isShared: true,
                    level: 0, // -1, 0, 1
                    status: 'done',
                    idSubproceso: '2998a23a92c73b83a7a73bfe1f9934b1',
                    codigoSubproceso: 'TareaResolucion'
                }, {
                    description: 'Documentos a aportar',
                    index: 0, // orden
                    ownerCode: '1001',
                    ownerDescription: ' - Gran Vía',
                    isEnabled: true,
                    isRequired: true,
                    isShared: true,
                    level: 0, // -1, 0, 1
                    status: 'undone',
                    idSubproceso: '066e4ca0556b135b2c36043bee6c7931',
                    codigoSubproceso: 'TareaDocumentosAportar'
                }]
            }
            ];

        }
        
    })();

(function() {
  'use strict';
 
  var moduleDependencies = [
      'ui.router',
      'fondoinvers.core',
      'bankia.ui.tasklist',
      'bankia.ui.button'
  ];

  /**
   * @ngdoc overview
   * @name weatherApp.about
   * @requires ui.router
   *
   * @description
   * Module for the about section.
   */
  angular.module('fondoinvers.newDeal', moduleDependencies);
 
})();
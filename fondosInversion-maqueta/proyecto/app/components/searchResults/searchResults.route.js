(function() {
  'use strict';
  
  angular
  .module('fondoinvers.searchResults')
  .config(configuration);
  
  /* @ngInject */
  function configuration($stateProvider) {
    $stateProvider
    .state('main.searchResults', {
      url: '/searchResults',
      templateUrl: 'components/searchResults/searchResults.component.html',
      controller: 'searchResultsController',
      controllerAs: 'searchResultsCtrl'
    })
  }
})();
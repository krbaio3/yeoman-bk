(function() {
	'use strict';


	angular.module("fondoinvers.searchResults")
	.directive('repFondosDestinoTemplate', function() {
		return {
			restrict: 'E',
			templateUrl: 'components/searchResults/shared/dealDestFunds/dealDestFundsView.html'
		}
		
	});


})();
(function() {
	'use strict';


	angular.module("fondoinvers.searchResults")
	.directive('documentationInjectionTemplate', function() {
		return {
			restrict: 'E',
			templateUrl: 'components/searchResults/shared/docuInjection/docuInjectionView.html'
		}
		
	});


})();
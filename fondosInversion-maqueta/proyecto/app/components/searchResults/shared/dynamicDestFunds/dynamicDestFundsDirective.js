(function() {
	'use strict';


	angular.module("fondoinvers.searchResults")
	.directive('dinFondosDestinoTemplate', function() {
		return {
			restrict: 'E',
			templateUrl: 'components/searchResults/shared/dynamicDestFunds/dynamicDestFundsView.html'
		}
		
	});


})();
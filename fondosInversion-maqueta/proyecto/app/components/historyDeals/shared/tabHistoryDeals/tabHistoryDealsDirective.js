(function() {
	'use strict';


	angular.module("fondoinvers.historyDeals")
	.directive('tabHistoryDealsTemplate', function() {
		return {
			restrict: 'E',
			templateUrl: 'components/historyDeals/shared/tabHistoryDeals/tabHistoryDealsView.html'
		}
		
	});


})();
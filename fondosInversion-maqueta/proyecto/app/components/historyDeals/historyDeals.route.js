(function() {
  'use strict';
  
  angular
  .module('fondoinvers.historyDeals')
  .config(configuration);
  
  /* @ngInject */
  function configuration($stateProvider) {
    $stateProvider
    .state('main.historyDeals', {
      url: '/historyDeals',
      templateUrl: 'components/historyDeals/historyDeals.component.html',
      controller: 'historyDealsController',
      controllerAs: 'historyDealsCtrl'
    })
  }
})();
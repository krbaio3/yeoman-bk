(function() {
  'use strict';
  
  var moduleDependencies = [
  'ui.router',
  'fondoinvers.core'
  ];

  /**
   * @ngdoc overview
   * @name weatherApp.about
   * @requires ui.router
   *
   * @description
   * Module for the about section.
   */
   angular.module('fondoinvers.historyDeals', moduleDependencies);
   
 })();
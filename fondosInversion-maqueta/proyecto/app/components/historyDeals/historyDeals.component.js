(function () {
  'use strict';

  angular
  .module('fondoinvers.historyDeals')
  .controller('historyDealsController', historyDealsController);


  function historyDealsController($scope, $state, serviciosEjemplo){

    var vm = this;
    vm.active;

    $scope.serviceDatosCliente = serviciosEjemplo.getDatosCliente();
    

    vm.historyDealsArray = serviciosEjemplo.getHistoricoRepartos();

    vm.changeClass = function (indexRow) {

      vm.active = indexRow;
      vm.habilitarBotonDetalle = true;
    }

    /*vm.comprobarhistoryDeals = function () {
      vm.historyDealsArrayFin = new Array();

      for (var i = 0; i < vm.historyDealsArray.length; i++) {

        if (vm.datosCliente.nif == vm.historyDealsArray[i].nif) {
          vm.historyDealsArrayFin.push(vm.historyDealsArray[i]);
        }

      };


    }*/

  }
  
})();

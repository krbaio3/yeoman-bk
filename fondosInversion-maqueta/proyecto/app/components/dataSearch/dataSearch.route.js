(function() {
  'use strict';
  
  angular
  .module('fondoinvers.dataSearch')
  .config(configuration);
  
  /* @ngInject */
  function configuration($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('main.dataSearch', {
      url: '/dataSearch',
      templateUrl: 'components/dataSearch/dataSearch.component.html',
      controller: 'dataSearchController',
      controllerAs: 'dataSearchCtrl'
    })
  }
})();
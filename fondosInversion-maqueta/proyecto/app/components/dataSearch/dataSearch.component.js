  
(function () {
  'use strict';

  angular
  .module('fondoinvers.dataSearch')
  .controller('dataSearchController', dataSearchController);


  function dataSearchController($state, serviciosEjemplo){

    var vm = this;
    vm.tipoCliente = 'cliente';
    vm.pantalla = 'pantallaInicial';

    /* Variable booleana para mostrar/ocultar la tabla de resultados despues de chequear si existe el cliente */
    vm.mostrarResultados = false;
    /* Variable booleana para mostrar/ocultar  */
    //vm.finalizarResultados = false;
    /* Variable que guarda el estado de la ruta actual para el ruteo de las diferentes pantallas .. */
    vm.state = $state;
    /* Variable que guarda el tipo de entidad BANKIA ..*/
    vm.tipoEntidad;
    /* Variable que guarda el tipo de documento DNI, pasaporte ..*/
    vm.tipoDocumento;
    /* Variable para guardar la posición de la fila seleccionada en la tabla de "movimientos" del contrato */
    vm.active;
    vm.btnAceptar = true;
    vm.refCliente;
    vm.razonSocial;
    /* Variables ficticias para guardar el contrato */
    vm.iban;
    vm.num0;
    vm.num1;
    vm.num2;
    vm.num3;
    vm.numContrato;
    vm.familiaProducto;
    //Recojemos los objetos de ejemplo de 'familia de productos array'.
    vm.familiaProductosArray = serviciosEjemplo.getFamiliaProductos();
    //Recojemos los objetos de ejemplo de tipos de documentos.
    vm.tipoDocumentoArray = serviciosEjemplo.getTipoDocumentos();
    //Recojemos los objetos de ejemplo de tipos de entidades.
    vm.tipoEntidadArray = serviciosEjemplo.getTipoEntidades();
    //Recojemos los objetos de ejemplo de contratos.
    vm.historicoRepartosArray = serviciosEjemplo.getHistoricoRepartos(); 
    //Recojemos los datos de los clientes.
    vm.arrayClients = serviciosEjemplo.getClientes();

    vm.ver ="";


    vm.seeVariable = function () {

      vm.refCliente = '';
      vm.razonSocial = '';
      vm.checkSearch = false;
      vm.mostrarResultados = false;
      vm.finalizarResultados = false;
      vm.btnAceptar = true;
      
    }


    vm.comprobarContrato = function () {

      if (vm.familiaProducto==vm.familiaProductosArray[0]) {

            //Generamos de forma manual el numero de contrato introducido.
            vm.numContrato = (vm.iban+vm.num0+vm.num1+vm.num2+vm.num3);
            
            for (var i = 0; i < vm.arrayClients.length; i++) {

              if (vm.numContrato == vm.arrayClients[i].numero_contrato) {

                var client = [{
                  nif: vm.arrayClients[i].nif,
                  causante: vm.arrayClients[i].causante,
                  fondo_origen: vm.arrayClients[i].fondo_origen,
                  numero_contrato: vm.arrayClients[i].numero_contrato,
                  reparto: vm.arrayClients[i].reparto,
                  fecha_reparto: vm.arrayClients[i].fecha_reparto,
                  entidad: vm.arrayClients[i].entidad,
                  isin: vm.arrayClients[i].isin
                }];
                serviciosEjemplo.setDatosCliente(client);
                $state.go('searchResults');
                break;
              }
            }
          }
        }

        vm.changeClass = function (indexRow) {
          vm.active = indexRow;
          vm.finalizarResultados = true;
          vm.btnAceptar = false;
        }

        vm.checkOptSelected = function () {
          if(vm.active!=null) {

            vm.numContrato = (vm.iban+vm.num0+vm.num1+vm.num2+vm.num3);
            for (var i = 0; i < vm.arrayClients.length; i++) {
              if ((vm.refCliente == vm.arrayClients[i].nif) || (vm.numContrato == vm.arrayClients[i].numero_contrato)) {
                var client = [{
                  nif: vm.refCliente,
                  causante: vm.arrayClients[i].causante,
                  fondo_origen: vm.arrayClients[i].fondo_origen,
                  numero_contrato: vm.arrayClients[i].numero_contrato,
                  reparto: vm.arrayClients[i].reparto,
                  fecha_reparto: vm.arrayClients[i].fecha_reparto,
                  entidad: vm.arrayClients[i].entidad,
                  isin: vm.arrayClients[i].isin
                }];
                serviciosEjemplo.setDatosCliente(client);
                $state.go('searchResults');
                break;
              }

            }
            
          } 
        }


        vm.checkTipoDocu = function (tipo_ref) {

          if (tipo_ref == 0) {
            if (vm.tipoDocumento== 'DNI' && vm.tipoEntidad == 'BANKIA')  {
             console.log("antes dni check");
             vm.checkHistoricoRepartos('nif', vm.refCliente);
           }
         } else {

          vm.numContrato = (vm.num0+vm.num1+vm.num2+vm.num3);

          if (vm.familiaProducto == 'AHORRO VISTA/ PLAZO') {
            vm.checkHistoricoRepartos('numero_contrato', vm.numContrato);

          }
        } 
      }

      vm.showTable = function(){
       vm.mostrarResultados = true;
       vm.razonSocial= vm.historicoRepartosArray[0].razon_social;
      }

      /*vm.checkHistoricoRepartos = function (tipoDocu, refBusqueda) {


        for (var i = 0; i < vm.historicoRepartosArray.length; i++) {
          if (tipoDocu == 'nif') {
            if (vm.historicoRepartosArray[i].nif == refBusqueda) {
              vm.razonSocial = vm.historicoRepartosArray[i].razon_social;      
              vm.mostrarResultados = true;
              break;

            } else {
              vm.razonSocial = '';
              vm.mostrarResultados = false;
              vm.historicoRepartosArrayFin = new Array();
            }
          } else {

            if ((vm.historicoRepartosArray[i].num_contrato == refBusqueda) && (vm.historicoRepartosArray[i].iban == vm.iban)) {
              vm.razonSocial = vm.historicoRepartosArray[i].razon_social;
              vm.mostrarResultados = true;
              break;

            } else {
              vm.razonSocial = '';
              vm.mostrarResultados = false;
              vm.historicoRepartosArrayFin = new Array();
            }
          }
        };

        vm.historicoRepartosArrayFin = new Array();
        if (tipoDocu == 'nif') {
          for (var i = 0; i < vm.historicoRepartosArray.length; i++) {
            if (vm.historicoRepartosArray[i].nif == refBusqueda) {
              vm.historicoRepartosArrayFin.push(vm.historicoRepartosArray[i]);
            }
          };
        } else {
          for (var i = 0; i < vm.historicoRepartosArray.length; i++) {
            if ((vm.historicoRepartosArray[i].num_contrato == refBusqueda) && (vm.historicoRepartosArray[i].iban == vm.iban)) {
              vm.historicoRepartosArrayFin.push(vm.historicoRepartosArray[i]);
            }
          };
        }  
      }*/
      
      vm.aceptarTransicion = function () {
        $state.go('searchResults');
      }


      vm.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
          $event.preventDefault();
        }
      };
      
    }

  })();



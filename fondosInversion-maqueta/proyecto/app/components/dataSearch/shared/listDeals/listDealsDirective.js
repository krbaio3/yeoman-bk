(function() {
  'use strict';


angular.module("fondoinvers.dataSearch")
.directive('listDealsTemplate', function() {
  return {
	  	strict: 'E',
	    templateUrl: 'components/dataSearch/shared/listDeals/listDealsView.html'
    }
  
});


})();
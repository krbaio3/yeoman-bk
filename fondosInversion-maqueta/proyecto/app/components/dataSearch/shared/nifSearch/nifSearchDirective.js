(function() {
	'use strict';


	angular.module("fondoinvers.dataSearch")
	.directive('nifSearchTemplate', function() {

		function link(scope, elem, attrs){
			$("#documento").keyup(function() {
				if (this.value.length == this.maxLength) {
					$('#buscar').focus();
				}
			});
		}

		return {
			strict: 'E',
			templateUrl: 'components/dataSearch/shared/nifSearch/nifSearchView.html',
			link: link
		}

	});

})();
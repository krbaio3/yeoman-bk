(function() {
  'use strict';


angular.module("fondoinvers.dataSearch")
.directive('typeDocuTemplate', function() {
  return {
	  	strict: 'E',
	    templateUrl: 'components/dataSearch/shared/typeDocument/typeDocuView.html'
    }
  
});


})();
(function() {
	'use strict';
	angular.module("fondoinvers.dataSearch")
	.directive('ibanSearchTemplate', function() {

		function link(scope, elem, attrs){
			$( document ).ready(function() {
				$(".inputs").keyup(function() {
					if (this.value.length == this.maxLength) {
						var nextItem = $(this).next('.inputs');
						if(nextItem.length){
							if (!$(nextItem).attr("disabled")) {
								$(this).css('background', '#fff');
								nextItem.focus();
							} else {
								$(this).next('.inputs').next('.inputs').focus();
								$(this).css('background', '#fff');
								var n =  $(this).next('.inputs');
							}
						}else{
							$(this).css('background', '#fff');
							$(this).blur();
							$('#botonVer').focus();
						}

					}
				});
			});
		}

		return {
			restrict: 'E',
			templateUrl: 'components/dataSearch/shared/ibanSearch/ibanSearchView.html',
			link: link
		}
	});
})();
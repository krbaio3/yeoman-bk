(function() {
  'use strict';


angular.module("fondoinvers")
.directive('footerTemplate', function() {
  return {
	  	strict: 'E',
	    templateUrl: 'shared/footer/footerView.html',
    }
  
});


})();
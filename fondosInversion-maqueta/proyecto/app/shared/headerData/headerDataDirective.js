(function() {
	'use strict';


	angular.module("fondoinvers")
	.directive('headerDataTemplate', function() {
		return {
			restrict: 'E',
			templateUrl: 'shared/headerData/headerDataView.html'
		}
		
	});


})();

(function () {
  'use strict';


angular
  .module('fondoinvers', [
    'fondoinvers.dataSearch',
    'fondoinvers.historyDeals',
    'fondoinvers.searchResults',
    'fondoinvers.newDeal'
    ])
  .controller('mainController', function(){
  	var vm = this;
  	vm.fromStateFooter = null;
  	vm.toStateFooter = null;
    vm.footerCase = 0;
  	vm.setState = function(fromState, toState){
	    vm.fromStateFooter = fromState;
  		vm.toStateFooter = toState;	
      if(vm.fromStateFooter == 'dataSearch' && vm.toStateFooter =="searchResults"){
        vm.footerCase = 1;
      }else  if(vm.fromStateFooter == 'searchResults' && vm.toStateFooter =="historyDeals"){
        vm.footerCase = 2;
      }
      else  if(vm.fromStateFooter == 'historyDeals' && vm.toStateFooter =="searchResults"){
        vm.footerCase = 3;
      }else {
        vm.footerCase = 0;
      }
  		console.log("valores setesados a : to:"+ vm.toStateFooter+" from:"+vm.fromStateFooter, vm.footerCase)	;
	  }
  })

  .config(configuration);

    function configuration($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('main',{
    	abstract: true,
    	templateUrl: 'shared/main.html',
  		controller: 'mainController',
      controllerAs: 'mainCtrl'
  	})
    $urlRouterProvider.otherwise('/dataSearch');
  }
  // .run(function($rootScope){
  // 	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
 
  // 		if((fromState.name == 'app.dataSearch') && (toState.name == 'app.searchResults')){
  // 			console.log("okey");
  // 		}
  // 	})
  // })
  

})();
# Generador de proyectos Bankia Base utilizando GULP

Crea la estructura para un nuevo proyecto que utilice la arquitectura AAD de componentes Angular.

## Uso

###Asegurarse de disponer Yeoman instalado. Si no estuviera instalado, ejecutar el comando:

```
npm install -g yo
```

###Instalar el generador

```
npm install -g git+https://<username>@aqdes-stash.cm.es/stash/scm/aicli/generator-bankia-base-gulp.git
```

### Generar un proyecto

```
yo bankia-base-gulp
```

### Instalar dependencias

```
bower install
```

### Ejecutar aplicación

```
$ gulp server
```

### Ejecutar los test

```
$ gulp test
```

########## Resumen v0.1 ##########
```
Cómo está estructurado el proyecto:
        + app:    carpeta principal del proyecto, contiene el código de la application. Está dividida en:
            - components: componentes de la aplicación, que a su vez están divididos en módulos,
                * modulos:  contienen los JS, SCSS y PUG.
            - index.jade: PUG principal donde se cargan todos los PUG y JS de los componentes
        + shared: componentes distribuidos
        + assets: estáticos de la aplicación (imagenes, etc)
        + test:   carpeta donde están los test          

Cada archivo se tiene el siguiente propósito:
    +app/app.css: Estilos CSS comunes a toda la aplicación.
    +app/app.module.js: Definición del módulo principal de la aplicación.
    +app/app.config.js: Configuración del módulo principal de la aplicación.
    +app/app.route.js: Rutas generales de la aplicación.
    +app/blocks/: Contiene el conjunto de bloques o componentes de uso común en varias partes de la aplicación, como por ejemplo manejo de logs, seguridad, manejo de errores o eventos de +
      analytics.
    +app/core/; Contiene los servicios y constantes que hagan parte de la lógica de negocio de la aplicación.
    +app/core/core.module.js: Define el módulo "core" de la aplicación. En algunos casos, en especial si se tienen muchas dependencias, este módulo puede referenciar las dependencias que se
      usen en los demás módulos de la aplicación, así cada módulo en lugar de referenciar cada una de estas, simplemente usa este módulo.
    +app/core/constants.js: Allí se tendrán las constantes que sean de uso compartido entre varios módulos de la aplicación.
    +app/core/bussiness-logic.service.js: En caso de los servicios o factories, el archivo tendrá el sufijo ".service" y servirá para implementar una funcionalidad específica de la
      lógica de negocios o consultas a servicios externos.
    +app/core/bussiness-logic.service.spec.js: Cada elemento de AngularJS que se construya para la aplicación (directiva, controlador o servicio) puede tener pruebas unitarias, las cuales   
      deben estar en un archivo con el mismo nombre del elemento desarrollado, pero con el sufijo ".spec". Estas pruebas unitarias van en la misma carpeta que el elemento probado no sólo para que sea fácil de ubicar sino también para que sea más inmediato ver que al cambiar el archivo con el código de la aplicación sea más fácil recordar que se deben tener actualizados con los cambios en el código. Las pruebas de integración (end-to-end) deben ir en una carpeta por fuera de "app/", por ejemplo "tests/" o "e2e/".
    +app/index.html: AngularJS es un framework basado en el principio de Aplicaciones de una sola Página (Single-page application), por lo que este será el punto de entrada a la aplicación
    +app/layout/: Los elementos visuales que definen un diseño común a toda la aplicación estarán en esta carpeta.
    +app/section1|2: Cada funcionalidad o sección dentro de la aplicación tendrá su carpeta en la raíz de "app/" y tendrá su propio módulo y rutas (si aplica), de esta manera es posible   
      tratar de una manera isolada cada sección como una aplicación web más pequeña.
    +app/section1|2/section1|2.controller.js: En caso de los controladores, el archivo tendrá el sufijo ".controller" y sólo tendrá definida un controlador por archivo.
    +app/section1|2/section1|2.css: Estilos CSS usados solamente en la sección.
    +app/section1|2/section1|2.module.js: Módulo de la sección.
    +app/section1|2/section1|2.route.js: Rutas de la sección.
    +app/section1/subsection1-1: Cada sección puede tener sus propias sub-secciones o si se tienen pocas, todos los archivos pueden estar dentro de la sección contenedora. Como preferencia
      personal sugiero que así sean pocos archivos cada sub-sección tenga su propia carpeta.
    +app/widgets/: Carpeta con componentes gráficos comunes a varias secciones o controladores de la aplicación, por ejemplo directivas de elementos de UI. En la documentación con las
      recomendaciones de la estructura de proyectos esta carpeta aparece con el nombre "components", pero en este caso se usa "widgets" para evitar futuras confusiones dado que Angular 2 está usando ese nombre con otro propósito.
```

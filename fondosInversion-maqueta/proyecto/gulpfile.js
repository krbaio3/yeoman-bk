'use strict';

var gulp            = require('gulp');
var open            = require('gulp-open');
//var jade            = require('gulp-jade'); //delete this
var browserSync     = require('browser-sync').create();
var useref          = require('gulp-useref');
var gulpIf          = require('gulp-if');
var uglify          = require('gulp-uglify');
var gutil           = require('gulp-util');
var cssnano         = require('gulp-cssnano');
var inject          = require('gulp-inject');
var plumber         = require('gulp-plumber');
var watch           = require('gulp-watch');
var scsslint        = require('gulp-scss-lint');
var print           = require('gulp-print');
var cleanCSS        = require('gulp-clean-css');
var minify          = require('gulp-minify');
var angularFilesort = require('gulp-angular-filesort');
var sass            = require('gulp-sass');
var debug           = require('gulp-debug');
var sourcemaps      = require('gulp-sourcemaps');
var bowerFiles      = require('main-bower-files');
var copy            = require('gulp-copy');
//injecta múltiples streams diferentes
var es = require('event-stream');
var connect = require('gulp-connect');

//Limpia capeta de construccion
var del             = require('del');
//Reglas de calidad y estilo JSHINT y JSCS
var jshint          = require('gulp-jshint');
var jscs            = require('gulp-jscs');
//Inyeccion de Bower
var wiredep         = require('wiredep').stream;
//Transforma SASS a CSS
var compass         = require ('gulp-compass');
//Usamos runSequence porque es la versión 3.9.x de Gulp, cuando se suba de versión
//se debe actualizar esta tarea con la nueva versión. Esto es una lista de tareas
//que se deben ejecutar secuencialmente
var runSequence     = require('run-sequence');

//Karma
var karmaServer     = require ('karma').Server;
/* TODO LA PARTE DE TEST NO ESTÁ TERMINADA. HAY QUE REHACERLA 23/08/2016*/

//Declaracion de Variables

var base = {
  app: require('./bower.json').appPath || 'app',
  dist: './dist',
  assets: ('./assets'),
};

var baseDir = {
  pug: base.app + '/index.html', // change .html
  components: base.app + '/components'
}

var path = {
  scripts: [base.app + '/components/**/*.js'],
  scriptsAll: [base.app + '/**/*.js'],
  styles: [base.app + '/components/**/*.{sass,scss}'],
  stylesAll: [base.app + '/**/*.{sass,scss}'],
  assetsScripts: [base.assets + '/**/*.js'],
  assetsStyles: [base.assets + '/**/*.{sass,scss}'],
  assetsViews: [base.assets + '/**/*.{jade,pug}'],
  assetsImg: './app/assets/img/**/*.*',
  sharedStyles: [base.app + '/shared/**/*.{sass,scss}'],
  distScripts: [base.dist + '/**/*.js'],
  distStyles: [base.dist + '/**/*.{sass,scss}'],
  distImg: base.dist + '/assets/img/',
  distViews : base.dist + '/assets/views/',
  distAssets : base.dist + '/assets/',
  test: ['test/spec/**/*.js'],
  testRequire: [
    base.app + '/bower_components/angular/angular.js',
    base.app + '/bower_components/angular-mocks/angular-mocks.js',
    base.app + '/bower_components/angular-resource/angular-resource.js',
    base.app + '/bower_components/angular-cookies/angular-cookies.js',
    base.app + '/bower_components/angular-sanitize/angular-sanitize.js',
    base.app + '/bower_components/angular-route/angular-route.js',
    'test/mock/**/*.js',
    'test/spec/**/*.js'
  ],
  karma: 'karma.conf.js',
  views: {
    main: baseDir.pug,
    files: [base.app + '/components/**/*.html'], // change to .html
    filesAll: [base.app + '/**/*.html'] // change to .html
  },
  bower: './bower.json'
};

// Edit this values to best suit your app
var WEB_PORT = 9898;
var APP_DIR = 'app';

//Fin Variables

//funciones
var jadeFn = function() {
  return gulp.src(path.views.filesAll)
    .pipe(debug())
    .pipe(plumber())
    .pipe(print())
    //.pipe(jade()) //delete this pipe
    .pipe(gulp.dest(base.dist))
    .pipe(browserSync.stream());
};

//Fin funciones

/****
            INICIO TAREAS
******/

gulp.task('default', ['server']);

// TODO: revisar completa tarea build
// Production build
gulp.task('build', function (done) {
  runSequence('assets:img', 'copy:bower', 'copy:js','compass', 'jade', 'inject', done);
});

//gulp.task('build', ['clean', 'pug', 'compass', 'scripts', 'copy:bower-components', 'copy:fonts', 'copy:images', 'copy:data', 'copy:pdf-samples']);

// Static Server + watching scss/html files
gulp.task('server', ['build'], function() {
    browserSync.init({
        //files: "dist/**/*.*",
        server: {
          baseDir: "./dist"
        },
        defaultFile: "index.html",
        browserSync: true,
        port: WEB_PORT,
        open: "local"
    });

    gulp.src('index.html', {cwd: base.dist})
        .pipe(open({app: 'chrome', uri: 'localhost:9898'}));

    gulp.watch(path.stylesAll, ['compass','inject'], function(event){
      console.log('Event type: ' + event.type); // added, changed, or deleted
      console.log('Event path: ' + event.path); // The path of the modified file
    });

    gulp.watch(path.views.filesAll, ['jade','inject'], function(event) {
      console.log('Event type: ' + event.type); // added, changed, or deleted
      console.log('Event path: ' + event.path); // The path of the modified file
    });

    gulp.watch(path.bower, ['wiredep','inject'], function (event){
      console.log('Event type: ' + event.type); // added, changed, or deleted
      console.log('Event path: ' + event.path); // The path of the modified file
    });

    gulp.watch(path.scriptsAll, ['copy:js','inject'], function(event){
      console.log('Event type: ' + event.type); // added, changed, or deleted
      console.log('Event path: ' + event.path); // The path of the modified file
    });
    // gulp.watch("./app/**/*").on('change', browserSync.reload);
    gulp.watch("./dist/index.html").on('ready', browserSync.reload);
   
});

gulp.task('serve:dist', ['build'], function () {
  browserSync.init({
      server: {
        baseDir: './dist/'
      },
      defaultFile: 'index.html'
  });
});

        /***  BOWER ***/

// Inject libraries via Bower in between of blocks "bower:xx" in index.html
gulp.task('wiredep', function () {
  return gulp.src('index.html', {cwd: './dist'})
    .pipe(debug())
    .pipe(plumber({
      errorHandler: function (err) {
        this.emit('end');
        console.error(err);
      }
     }))
    .pipe(wiredep({
      //'directory': './bower_components',
      'ignorePath': '../',
      onError: function(err) {
        console.log(err)
      }
    }))
    .pipe(gulp.dest('./dist'))
    .pipe(browserSync.stream());
});

gulp.task('copy:bower', function () {
  gutil.log('Copiando carpeta bower_components')

  return gulp.src(['bower_components/**/*'], {
        base: './'
    })
    .pipe(debug())
    .pipe(plumber({
      errorHandler: function (err) {
      this.emit('end');
      console.error(err);
      }
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('copy:watch', function(){
  gulp.watch('bower_components', ['copy']);
});


        /**** FIN BOWER ****/

// Cleans the dist folder
gulp.task('clean:dist', function () {
  gutil.log('Borrando directorio ' + base.dist);
  //del('./dist/**/*.*');
  return del('./dist/**/*');
});

// Compress into a single file the ones in between of blocks "build:xx" in index.html
gulp.task('compress', ['wiredep', 'compass'], function () {
  return gulp.src(baseDir.pug)
    .pipe(useref({ searchPath: ['../' + base.assets] }))
    .pipe(gulpIf(path.distScripts, uglify({
      mangle: true
    }).on('error', gutil.log)))
    .pipe(gulpIf(path.distStyles, cssnano()))
    .pipe(gulp.dest(base.dist));
});

            /** SASS **/

//gulp.task('compass', ['clean:dist', 'scss-lint'], function () {
//gulp.task('compass', ['scss-lint'], function () {
gulp.task('compass', function () {
  return gulp.src('./app/sass/**/*.{sass,scss}')
      .pipe(debug())
      .pipe(plumber({
        errorHandler: function (/*err*/) {
          this.emit('end');
        }
       }))
      .pipe(compass({
        style: 'expanded',
        comments: 'description',
        css: './dist/css/',
        sass: './app/sass'
      }))
      .pipe(gulp.dest('./dist/css/'))
      .pipe(browserSync.stream());
});

gulp.task('compass-shared', ['scss-lint'], function () {

  return gulp.src(path.sharedStyles)
      .pipe(debug())
      .pipe(plumber({
        errorHandler: function (err) {
        this.emit('end');
        console.error(err);
        }
      }))
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./dist/shared'))
      .pipe(browserSync.stream());
});

gulp.task('compass:watch', function () {
  gulp.watch(path.styles, ['compass']);
});

gulp.task('compass-shared:watch', function () {
  gulp.watch('app/shared/**/*.{sass,scss}', ['compass-shared']);
});

            /** PUG **/

//Transform PUG to HTML & auto-inject into browsers
//gulp.task('jade', ['clean:dist'], jadeFn);
gulp.task('jade', jadeFn);


// Looks for code correctness errors in SASS and prints them
gulp.task('scss-lint', function() {
  return gulp.src(path.styles)
    .pipe(plumber())
    .pipe(scsslint());
});
          //**JS**//

gulp.task('copy:js', function () {
  gutil.log('Copiando carpetas App');
    //return gulp.src(['bower_components/**/*.{js,css}', '!bower_components/**/*.min.js*'], {
    return gulp.src(
        path.scriptsAll,
        {read:true})
    .pipe(gulp.dest(base.dist));
});

// Search for js and css files created for injection in index.html
gulp.task('inject', function () {

  var injectStyles = '';

  var injectScripts = '';


  var wiredepOptions = {
   //directory: '../bower_components',
   'ignorePath': '..'
  };

  return gulp.src('index.html', {cwd: './dist'})
    .pipe(debug())
    .pipe(plumber())
    .pipe(inject(gulp.src(
         './dist/**/*.css',
         {read: true}),
         {relative:true})
       )
        .pipe(gulp.dest(base.dist))

    .pipe(inject(gulp.src(
        ['./dist/**/*.js', '!./dist/bower_components/**/**'],
        {read:true}).pipe(angularFilesort()),
        {relative:true})
      )
    .pipe(gulp.dest(base.dist))
    .pipe(wiredep({
      'ignorePath': '../'
    }))
    // write the injections to the .tmp/index.html file
    .pipe(gulp.dest('./dist'))
    .pipe(browserSync.stream())
    // so that src/index.html file isn't modified
    // with every commit by automatic injects
});

//Validate-Code task
gulp.task('lint', ['jshint', 'jscs']);

// Looks for code correctness errors in JS and prints them
gulp.task('jshint', function() {
  return gulp.src(path.scriptsAll)
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

// Looks for code style errors in JS and prints them. Reporting & fixing & failing on lint error
gulp.task('jscs', function () {
  return gulp.src(path.scripts)
    .pipe(plumber())
    .pipe(jscs({fix: true}))
    .pipe(jscs.reporter())
    .pipe(jscs.reporter('fail'))
    //.pipe(gulp.dest('./dist'));
});

gulp.task('vet', function() {

  gutil.log('Analyzing source with JSHint and JSCS and FIX with JSCS');

  return gulp.src(path.scriptsAll)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish', { verbose: true }))
    .pipe(jshint.reporter('fail'))
    .pipe(jscs({fix: true}))
    .pipe(jscs.reporter())
    .pipe(jscs.reporter('fail'))
});

/****
            FIN TAREAS
******/

/*******
            ASSETS
********/

// Build all assets into the dist folder
//gulp.task('build:assets', ['assets:scripts', 'assets:styles', 'assets:views', 'assets:img']);
gulp.task('build:assets', ['assets:styles', 'assets:img']);

// build the assets styles into the dist folder
gulp.task('assets:styles', function () {

  return gulp.src(path.assetsStyles)
    .pipe(debug())
    .pipe(plumber({
      errorHandler: function (err) {
        this.emit('end');
        console.error(err);
      }
    }))
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/assets/css/'))
    .pipe(browserSync.stream());
});

// build the assets scripts into the dist folder
gulp.task('assets:scripts', function () {
  return gulp.src(path.assetsScripts)
  .pipe(plumber({
    errorHandler: function (err) {
      this.emit('end');
      console.error(err);
    }
   }))
  .pipe(inject(
    gulp.src(path.assetsScripts, {read: false}), {
      relative: true
    })).pipe(gulp.dest(path.distScripts))

});

// build the assets views into the dist folder
gulp.task('assets:views', function () {
  return gulp.src(path.assetsViews)
  .pipe(plumber({
    errorHandler: function (err) {
      this.emit('end');
      console.error(err);
    }
   }))
   .pipe(jade())
   .pipe(gulp.dest(path.distViews))
});

// build the assets img into the dist folder
gulp.task('assets:img', function () {
   gutil.log('copying images to ./dist');

   return gulp.src('./app/assets/img/**/*')
     .pipe(debug())
     .pipe(plumber())
     .pipe(print())
     .pipe(gulp.dest(path.distImg));
  });

/*******
            FIN ASSET
********/
